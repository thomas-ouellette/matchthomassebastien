package com.dawson.matchthomassebastien

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ImageButton
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.dawson.matchthomassebastien.R.layout

/**
 * Assignment 1: Match Game
 * This class will allow a user to play a match game, where they have to identify the outlier image. There are two sets
 * of images, one of five cats with a dog outlier, and one of five rabbits with a fox outlier. The user will score every
 * time they match the correct one. A miss counter will go up every time they choose the wrong one, and doing so twice
 * will cause the game to end and force them to restart. A counter of the number of the games they have played (determined
 * if they have matched the outlier or missed twice) will determine which set of images will appear when they reset the
 * game. The game always starts with the cat and dog images.
 *
 * @author Sebastien Palin
 * @author Thomas Ouellette
 */
class MainActivity : AppCompatActivity() {

    internal var missCounter = 0
    internal var matchCounter = 0
    internal var gameCounter = 0
    internal var resetCounter = 0

    internal lateinit var gamesPlayedText : TextView

    internal var outlierResource : Int = 0
    internal var outlierResourceHighlight : Int = 0
    internal var outlierImageButtonIndex : Int = 0

    internal var imageButtonArray : Array<ImageButton?> = arrayOfNulls(6)
    internal var imageArray = IntArray(6)
    /**
    * Function that is run when the Activity instance is created. It will first set the view to the main activity.
    * Next, it will load all the ImageButtons in the layout into an imageButtonArray, so that this class can set and
    * get their tags and change their image resource. 
    * It will also load in all the cat and dog images into an imageArray. This is done so that the ImageButtons can have
    * their images changed with the ease of accessing this array. It also saves the resource of the outlier image and the highlighted
    * outlier image, so that the image that is clicked can be compared to this variable to check if the user has indeed clicked
    * the correct image.
    * It will then check if the instance state bundle is not null. If it isn't, it will run a function to get all the bundle information. It will
     * also set the text of the games played View
    * @see getSavedInstanceState(savedInstanceState : Bundle)
    * If there is np bundle, it will shuffle the images in the view 
    * @see shuffleImageArray()
    */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layout.activity_main)

        gamesPlayedText = findViewById(R.id.gamesPlayedView)

        imageButtonArray[0] = findViewById(R.id.button1) as ImageButton
        imageButtonArray[1] = findViewById(R.id.button2) as ImageButton
        imageButtonArray[2] = findViewById(R.id.button3) as ImageButton
        imageButtonArray[3] = findViewById(R.id.button4) as ImageButton
        imageButtonArray[4] = findViewById(R.id.button5) as ImageButton
        imageButtonArray[5] = findViewById(R.id.button6) as ImageButton

        imageArray[0] = R.drawable.cat_1
        imageArray[1] = R.drawable.cat_2
        imageArray[2] = R.drawable.cat_3
        imageArray[3] = R.drawable.outlier_dog
        imageArray[4] = R.drawable.cat_4
        imageArray[5] = R.drawable.cat_5
        outlierResource = R.drawable.outlier_dog
        outlierResourceHighlight = R.drawable.outlier_dog_highlighted

        //Check if there is an instance state
        if(savedInstanceState != null){
            getSavedInstanceState(savedInstanceState)
        }
        //if there is no instance state, set all counters to sharedprefernces
        else{
            shuffleImageArray()
            getSharedPreferences()
        }

        gamesPlayedText.text = gameCounter.toString()
    }

    /**
    * Private function that will get everything needed from the saved instance state if there is a Bundle available
    * in onCreate. It will set both match and miss counter, as well as set the tag and image resources of every
    * ImageButton in imageButtonArray[]
    *
    * @param savedInstanceState  The Bundle containing all the instance information. Cannot be null, as that is checked
    *                            in the onCreate() method
     */
    private fun getSavedInstanceState(savedInstanceState :Bundle){
        //Get counters
        matchCounter = savedInstanceState.getInt(getString(R.string.matchCounterKey))
        missCounter = savedInstanceState.getInt(getString(R.string.missCounterKey))
        resetCounter = savedInstanceState.getInt(getString(R.string.resetCounterKey))
        gameCounter = savedInstanceState.getInt(getString(R.string.gameCounterKey))

        //Get outlier information
        outlierImageButtonIndex = savedInstanceState.getInt(getString(R.string.outlierIndexKey))
        outlierResource = savedInstanceState.getInt(getString(R.string.outlierResourceKey))
        outlierResourceHighlight = savedInstanceState.getInt(getString(R.string.outlierHighlighKey))

        //Set ImageButton images and tags
        imageButtonArray[0]?.setImageResource(savedInstanceState.getInt(getString(R.string.button1Key)))
        imageButtonArray[0]?.setTag(savedInstanceState.getInt(getString(R.string.button1Key)))
        imageButtonArray[1]?.setImageResource(savedInstanceState.getInt(getString(R.string.button2Key)))
        imageButtonArray[1]?.setTag(savedInstanceState.getInt(getString(R.string.button2Key)))
        imageButtonArray[2]?.setImageResource(savedInstanceState.getInt(getString(R.string.button3Key)))
        imageButtonArray[2]?.setTag(savedInstanceState.getInt(getString(R.string.button3Key)))
        imageButtonArray[3]?.setImageResource(savedInstanceState.getInt(getString(R.string.button4Key)))
        imageButtonArray[3]?.setTag(savedInstanceState.getInt(getString(R.string.button4Key)))
        imageButtonArray[4]?.setImageResource(savedInstanceState.getInt(getString(R.string.button5Key)))
        imageButtonArray[4]?.setTag(savedInstanceState.getInt(getString(R.string.button5Key)))
        imageButtonArray[5]?.setImageResource(savedInstanceState.getInt(getString(R.string.button6Key)))
        imageButtonArray[5]?.setTag(savedInstanceState.getInt(getString(R.string.button6Key)))

        //Get clickable state of each image
        imageButtonArray[0]!!.isClickable = savedInstanceState.getBoolean(getString(R.string.button1ClickKey))
        imageButtonArray[1]!!.isClickable = savedInstanceState.getBoolean(getString(R.string.button2ClickKey))
        imageButtonArray[2]!!.isClickable = savedInstanceState.getBoolean(getString(R.string.button3ClickKey))
        imageButtonArray[3]!!.isClickable = savedInstanceState.getBoolean(getString(R.string.button4ClickKey))
        imageButtonArray[4]!!.isClickable = savedInstanceState.getBoolean(getString(R.string.button5ClickKey))
        imageButtonArray[5]!!.isClickable = savedInstanceState.getBoolean(getString(R.string.button6ClickKey))
    }

    /**
     * Function that will get the shared preferences and set the counters to the values stored within
     */
    private fun getSharedPreferences(){
        val prefs = getPreferences(Context.MODE_PRIVATE)

        matchCounter = prefs.getInt(getString(R.string.matchCounterKey), 0)
        missCounter = prefs.getInt(getString(R.string.missCounterKey), 0)
        resetCounter = prefs.getInt(getString(R.string.resetCounterKey), 0)
        gameCounter = prefs.getInt(getString(R.string.gameCounterKey), 0)
    }

    /**
    * Private function that will shuffle imageArray/the array with all the images in it. Uses the Fisher-Yates
    * algorithm to do this
    *
    * @see: https://www.geeksforgeeks.org/shuffle-a-given-array-using-fisher-yates-shuffle-algorithm/ for more info
     */
    private fun shuffleImageArray(){
        var i = imageArray.size
        var j: Int
        var temp: Int

        while(i > 0){
            i -= 1
            j = Math.floor(Math.random() * (i+1)).toInt()

            temp = imageArray[i]
            imageArray[i] = imageArray[j]
            imageArray[j] = temp
        }

        shuffleImagesHelper()
    }

    /**
    * Helper method for the shuffleImageArray() method. This method will set the image resource of each imagebutton
    * in the imageButtonArray to the corresponding image in the imageArray.
    * Because the images are shuffled, the order in which this is accomplished is always the same
    * IE: [0] is always equals to [0], [1]=[1], etc.
     */
    private fun shuffleImagesHelper(){
        for (i in 0 until imageButtonArray.size){
            imageButtonArray[i]?.setImageResource(imageArray[i])
            imageButtonArray[i]?.setTag(i)

            //Get the position of the outlierResrouce in the array for easier use later
            if (imageArray[i] == outlierResource){
                outlierImageButtonIndex = i
            }
        }
    }

    /**
    * Function that is run when the user clicks on one of the match game images. It will create an ImageButton from
    * the View pased to it, the it will check the ImageButton's tag to see if it is the outlier image.
    * If it is: all the buttons become unclickable, the matchcounter and the games played counters go up by one,
    *           the outlier image is highlighted, and the user is prompted to start a new game.
    * If it isn't: The button pressed will become unclickable, the misscounter goes up by one, and the user is
    *               informed that their choice was wrong.
    * If the user misses twice: All buttons will become unclickable, the outlier becomes highlighted and a message
    *                           /alert pops up to tell the user they lost and to restart the game
    * In any case, the information of the button matching the outlier or not or if the user lost are logged and the View for displaying
     * the number of games played is changed to the current gameCounter.
    *
    * @param v  The View, which in this case is the ImageButton that was clicked.
     */
    fun imageClicked(v : View){
        val ib = v as ImageButton

        imageButtonArray[outlierImageButtonIndex]!!.setImageResource(outlierResourceHighlight)
        imageButtonArray[outlierImageButtonIndex]!!.setTag(outlierResourceHighlight)
        //If the user has selected the outlier, increase the match and game counters, set the view/ImageButton to the highlighted
        //outlier image, end the game, inform the user and log the event
        if (ib.getTag() == outlierImageButtonIndex) {
            Log.i(TAG, "HIT")
            matchCounter++
            gameCounter++
            gameOver()
            createWinDialog()

            Log.i(TAG, getString(R.string.outlierMatchedLog))
        }
        //The user hasn't selected the outlier, so increase the miss counter, set the button/view to disabled, pop up a toast informing the user
        //and log it
        else {
            Log.i(TAG, "MISS")
            missCounter++
            ib.isClickable = false
            // For some reason, popping up a toast her causes the application to fail...
            // Toast.makeText(this, getString(R.string.wrongButtonToast), Toast.LENGTH_SHORT).show()

            Log.i(TAG, getString(R.string.outlierNotMatchedLog))

            //If the miss counter is greater than 1, increase the game counter, end the game and tell the user
            if (missCounter > 1) {
                gameCounter++
                gameOver()
                createGameOverDialog()

                Log.i(TAG, getString(R.string.missTwoTimesLog))
            }
        }

        //Add to the gamesPlayed view if the gameCounter has gone up when clicking the button
        gamesPlayedText.text = gameCounter.toString()
    }

    fun openScores(v: View){
        val i = Intent(this, ScoresActivity::class.java)
        i.putExtra("timesplayed", this.gameCounter)
        i.putExtra("hits", this.matchCounter)
        i.putExtra("misses", this.missCounter)
        startActivity(i)
    }

    fun openAbout(v: View){
        startActivity(Intent(this, AboutActivity::class.java))
    }
    
    /**
     * Function that will create an alert dialog telling the user that they have won the game
     */
    private fun createWinDialog(){
        val winAlert = AlertDialog.Builder(this)
        winAlert.setMessage(getString(R.string.winMessage))
        winAlert.setTitle(getString(R.string.winTitle))
        winAlert.setPositiveButton(R.string.winButton, null)
        winAlert.create()
        winAlert.show()
    }

    /**
     * Function that will create an alert dialog telling the user that they have lost the game and must restart
     */
    private fun createGameOverDialog(){
        val lostAlert = AlertDialog.Builder(this)
        lostAlert.setMessage(getString(R.string.gameOverMessage))
        lostAlert.setTitle(getString(R.string.gameOverTitle))
        lostAlert.setPositiveButton(R.string.gameOverButton, null)
        lostAlert.create()
        lostAlert.show()
    }

    /**
     * Function that will set the outlier ImageButton to the highlighted outlier and sets the tag of said ImageButton to the highlighted as well.
     * This is to facilitate instance state saving as the highlighted outlier will be displayed.
     * Also sets every ImageButton to be unclickable
     */
    private fun gameOver(){
        for (i in 0 until imageButtonArray.size){
            imageButtonArray[i]!!.isClickable = false
        }
    }

    /**
     * Function that will set all the imageButtons to clickable
     */
    private fun gameRestart(){
        for (i in 0 until imageButtonArray.size){
            imageButtonArray[i]!!.isClickable = true
        }
    }

    /**
    * Function that will run when the user clicks on the reset button. The function will shuffle all of the images,
    * set all of them to a clickable state, and reset the misscounter. It will also log the event. Increments the resetCounter
    * by one and checks if it is an even or odd number. If it is odd, it will set the images to the rabbits and the fox. If it is
    * even, it will set the images to the cats and the dog. Also sets all the buttons to clickable
     * Functions used:
     * @see gameRestart() -> set images to clickable
     * @see shuffleImageArray() -> shuffle the images
    *
    * @param v  The View, in this case the button itself. This is not used, as the function has no need of it
     */
    fun resetButtonClicked(v : View){
        //Change images based on resetCounter value
        resetCounter++
        if (resetCounter % 2 == 0){
            imageArray[0] = R.drawable.cat_1
            imageArray[1] = R.drawable.cat_2
            imageArray[2] = R.drawable.cat_3
            imageArray[3] = R.drawable.outlier_dog
            imageArray[4] = R.drawable.cat_4
            imageArray[5] = R.drawable.cat_5
            outlierResource = R.drawable.outlier_dog
            outlierResourceHighlight = R.drawable.outlier_dog_highlighted
        }
        else{
            imageArray[0] = R.drawable.bunny_1
            imageArray[1] = R.drawable.bunny_2_2
            imageArray[2] = R.drawable.bunny_3
            imageArray[3] = R.drawable.outlier_fox
            imageArray[4] = R.drawable.bunny_4
            imageArray[5] = R.drawable.bunny_5
            outlierResource = R.drawable.outlier_fox
            outlierResourceHighlight = R.drawable.outlier_fox_highlighted
        }

        gameRestart()
        shuffleImageArray()
        missCounter = 0

        Log.i(TAG, getString(R.string.resetButtonLog))
    }

    /**
    * Function that will run when the user clicks on the zero button. The button will reset the miss, match, reset and game
    * counters. It will also reset the shared preferences counters. It will also update the View with games displayed.
     * This function logs that it has happened.
    *
    * @param v  The View, in this case the button itself. This is not used, as the function has no need of it
     */
    fun zeroButtonClicked(v : View){
        missCounter = 0
        matchCounter = 0
        resetCounter = 0
        gameCounter = 0

        //Update View with games played
        gamesPlayedText.text = gameCounter.toString()

        //Update shared preferences
        setSavedCountersToZero()

        Log.i(TAG,getString(R.string.zeroButtonLog))
    }

    /**
     * Function that sets all the counters in shared preferences to zero to reflect the zero button being clicked
     */
    private fun setSavedCountersToZero(){
        with (getPreferences(Context.MODE_PRIVATE).edit()){
            putInt(getString(R.string.matchCounterKey), 0)
            putInt(getString(R.string.missCounterKey), 0)
            putInt(getString(R.string.resetCounterKey), 0)
            putInt(getString(R.string.gameCounterKey), 0)
            commit()
        }
    }

    /**
    * Function that will save the instanced state of the game between any interuptions such as rotating the screen.
    * Saves the match and miss counters, as well as the image resource of every image button.
     * Saves the index of the outlier image in the image button array, the outlier resource and the highlighted outlier resource.
     * Saves the clickable state of each image button
    *
    * @param outState  The Bundle where all of the information is saved to before an interuption
     */
    public override fun onSaveInstanceState(outState : Bundle){

        outState?.run{
            //Save counters
            putInt(getString(R.string.matchCounterKey), matchCounter)
            putInt(getString(R.string.missCounterKey), missCounter)
            putInt(getString(R.string.resetCounterKey), resetCounter)
            putInt(getString(R.string.gameCounterKey), gameCounter)
            //Save the image of each ImageButton
            putInt(getString(R.string.button1Key), imageButtonArray[0]?.getTag() as Int)
            putInt(getString(R.string.button2Key), imageButtonArray[1]?.getTag() as Int)
            putInt(getString(R.string.button3Key), imageButtonArray[2]?.getTag() as Int)
            putInt(getString(R.string.button4Key), imageButtonArray[3]?.getTag() as Int)
            putInt(getString(R.string.button5Key), imageButtonArray[4]?.getTag() as Int)
            putInt(getString(R.string.button6Key), imageButtonArray[5]?.getTag() as Int)
            //Save the outlier index, resource and highlighted resource
            putInt(getString(R.string.outlierIndexKey), outlierImageButtonIndex)
            putInt(getString(R.string.outlierResourceKey), outlierResource)
            putInt(getString(R.string.outlierHighlighKey), outlierResourceHighlight)
            //Save the clickable state of each button
            putBoolean(getString(R.string.button1ClickKey), imageButtonArray[0]!!.isClickable)
            putBoolean(getString(R.string.button2ClickKey), imageButtonArray[1]!!.isClickable)
            putBoolean(getString(R.string.button3ClickKey), imageButtonArray[2]!!.isClickable)
            putBoolean(getString(R.string.button4ClickKey), imageButtonArray[3]!!.isClickable)
            putBoolean(getString(R.string.button5ClickKey), imageButtonArray[4]!!.isClickable)
            putBoolean(getString(R.string.button6ClickKey), imageButtonArray[5]!!.isClickable)
        }

        super.onSaveInstanceState(outState)
    }

    /**
     * Override for onStop so as to use shared preferences to save all the counters
     */
    public override fun onStop() {
        super.onStop()

        saveCounter()
    }

    /**
     * Function that uses shared preferences to save all the counters and restore them for a later date
     */
    private fun saveCounter(){
        with (getPreferences(Context.MODE_PRIVATE).edit()){
            putInt(getString(R.string.matchCounterKey), matchCounter)
            putInt(getString(R.string.missCounterKey), missCounter)
            putInt(getString(R.string.resetCounterKey), resetCounter)
            putInt(getString(R.string.gameCounterKey), gameCounter)
            commit()
        }
    }

    /**
    * An object that acts as the tag for logging purposes. It states that any log event has occurred in the main activity
     */
    companion object{
        private val TAG="Main_activity"
    }

}
