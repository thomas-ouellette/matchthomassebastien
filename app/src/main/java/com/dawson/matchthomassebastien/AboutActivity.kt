package com.dawson.matchthomassebastien

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

/**
 * Simple UI displaying description of the game, and credits.
 */
class AboutActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_about)
    }
}
