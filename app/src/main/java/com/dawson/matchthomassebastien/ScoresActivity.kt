package com.dawson.matchthomassebastien

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView

/**
 * UI for displaying various stats: the amount of games played,
 * and the amount of successful and unsuccessful guesses.
 *
 * @author Thomas Ouellette
 */
class ScoresActivity : AppCompatActivity() {

    private var timesplayed: Int = 0
    private var hits: Int = 0
    private var misses: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_scores)

        // Get the values of the counters from the intent
        this.timesplayed = intent.extras!!.getInt("timesplayed")
        this.hits = intent.extras!!.getInt("hits")
        this.misses = intent.extras!!.getInt("misses")

        displayCounters()
    }

    /**
     * Puts the value of each counter in its respective TextView
     */
    private fun displayCounters(){
        findViewById<TextView>(R.id.display_timesplayed).text = this.timesplayed.toString()
        findViewById<TextView>(R.id.display_hits).text = this.hits.toString()
        findViewById<TextView>(R.id.display_misses).text = this.misses.toString()
    }
}
